/**
 * Conqueue is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Conqueue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Conqueue.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONQUEUE_H
#define CONQUEUE_H

/* #define DEFAULT_CAPACITY 4096 */

#include <condition_variable>
#include <mutex>
#include <vector>

namespace mutils {
/**
 * This class represents a concurrent blocking queue. All the methods
 * are thread safe which means that they can be called from different
 * threads without any data inconsitency. The size of the queue
 * depends on the free memory on the system.
 */
template <typename T> class blockingqueue
{
  public:
  /**
   * Default destructor. When it is called the internal container is
   * cleaned.
   */
  ~blockingqueue();

  /**
   * It enqueues an element to the end of the queue. This method is
   * thread safe.
   *
   * @param el The element that should be queued.
   */
  void enqueue(T&& el);

  /**
   * It dequeues the first element in the queue. When the method has a
   * return value, that value has been removed from the queue. This
   * method is thread safe.
   *
   *
   * @return The first elment of the queue.
   * @throw std::runtime_error when the queue is empty.
   */
  T dequeue();

  /**
   * It dequeues the first element of the queue. In the case the queue
   * is empty, the the calling thread is blocked until the queue has
   * an element for it. When the method has a return value, that value
   * has been removed from the queue. This method is thread safe.
   *
   *
   * @return The first element of the queue.
   */
  T dequeueOrBlock();

  /**
   * It dequeues the first element of the queue. In the case of an
   * empty queue, the caller blocks for the given duration. It becomes
   * unblocked when at least one element is present in the queue or
   * the time has expired. When the method has a return value, that
   * value has been removed from the queue. This emthod is thread
   * safe.
   *
   * @param duration The time that the caller should wait for an element in the
   * queue.
   *
   * @return The first eleemnt of the queue, if there are any.
   * @throw std::runtime_error when the time has expired and the queue is empty.
   */
  T dequeueOrBlockFor(std::chrono::system_clock::duration);

  private:
  std::mutex _mx;
  std::condition_variable _cv;
  std::vector<T> _container;
};
}

template <typename T> mutils::blockingqueue<T>::~blockingqueue()
{
  _container.clear();
}

template <typename T> void mutils::blockingqueue<T>::enqueue(T&& el)
{
  std::lock_guard<std::mutex> lk(_mx);
  _container.emplace_back(std::forward<T>(el));
  _cv.notify_one();
}

template <typename T> T mutils::blockingqueue<T>::dequeue()
{
  std::lock_guard<std::mutex> lk(_mx);
  if (_container.begin() == _container.end()) {
    throw std::runtime_error("queue is empty");
  }
  auto it = _container.begin();
  _container.erase(it);
  return *it;
}

template <typename T> T mutils::blockingqueue<T>::dequeueOrBlock()
{
  std::unique_lock<std::mutex> uk(_mx);
  if (_container.begin() == _container.end()) {
    _cv.wait(uk, [this]() { return _container.begin() != _container.end(); });
  }
  auto it = _container.begin();
  _container.erase(it);
  return *it;
}

template <typename T>
T mutils::blockingqueue<T>::dequeueOrBlockFor(
    std::chrono::system_clock::duration duration)
{
  std::unique_lock<std::mutex> uk(_mx);
  if (_container.begin() == _container.end()) {
    _cv.wait_for(uk, duration);
  }

  if (_container.begin() == _container.end()) {
    throw std::runtime_error("time out on empty queue.");
  }

  auto it = _container.begin();
  _container.erase(it);
  return *it;
}

#endif
