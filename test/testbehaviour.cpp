#include "blockingqueue.h"
#include <catch.hpp>
#include <thread>

bool conexception = false;
int conreturn = 0;

SCENARIO("concurrent enqueue and dequeue", "[conqueue]")
{
  mutils::blockingqueue<int> queue;

  GIVEN("two threads, one consumer and one producer")
  {
    conexception = false;
    conreturn = 0;
    WHEN("consumer dequeues the empty queue")
    {
      std::thread consumer(
          [](mutils::blockingqueue<int>* q) {
            try {
              q->dequeue();
            } catch (std::exception& e) {
              conexception = true;
            }
          },
          &queue);
      THEN("an exception occures")
      {
        consumer.join();
        CHECK(conexception);
      }
    }

    WHEN("consumer pops before the producer has pushed an element")
    {
      conexception = false;
      conreturn = 0;
      std::thread consumer(
          [](mutils::blockingqueue<int>* q) {
            try {
              conreturn = q->dequeueOrBlock();

            } catch (std::exception& e) {
              conexception = true;
            }
          },
          &queue);

      std::thread producer(
          [](mutils::blockingqueue<int>* q) {
            try {
              std::this_thread::sleep_for(std::chrono::seconds(1));
              q->enqueue(5);
            } catch (std::exception& e) {
              conexception = true;
            }
          },
          &queue);

      THEN("consumer blocks until the producer has pushed the element")
      {
        CHECK(conreturn == 0);
        producer.join();
        consumer.join();
        CHECK(conreturn == 5);
      }
    }

    WHEN("consumer pops concurrently when the the producer is pushing an "
         "element")
    {
      conexception = false;
      conreturn = 0;
      std::this_thread::sleep_for(std::chrono::seconds(1));
      std::thread producer(
          [](mutils::blockingqueue<int>* q) {
            try {
              q->enqueue(5);
            } catch (std::exception& e) {
              conexception = true;
            }
          },
          &queue);

      std::thread consumer(
          [](mutils::blockingqueue<int>* q) {
            try {
              conreturn = q->dequeueOrBlock();

            } catch (std::exception& e) {
              conexception = true;
            }
          },
          &queue);
      THEN("consumer blocks until the producer has pushed the element")
      {
        producer.join();
        consumer.join();
        CHECK(conreturn == 5);
      }
    }

    WHEN("consumer pops and blocks 1s on an empty queue")
    {
      conexception = false;
      conreturn = 0;
      std::thread consumer(
          [](mutils::blockingqueue<int>* q) {
            try {
              std::chrono::seconds s(1);
              q->dequeueOrBlockFor(s);
            } catch (std::exception& e) {
              conexception = true;
            }
          },
          &queue);
      THEN("an exception occures")
      {
        consumer.join();
        CHECK(conexception);
      }
    }
  }
}

